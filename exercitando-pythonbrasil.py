#https://wiki.python.org.br/EstruturaSequencial

#1
print("Alo mundo")

#2
num = input("Digite um numero\n")
print(f'O número informado foi {num}.')

#3
num1 = int(input("Digite um numero\n"))
num2 = int(input("Digite um numero\n"))
soma = num1 + num2
print(f'O número informado foi {soma}.')

#4
num1 = float(input("Digite a 1 nota bimestral\n"))
num2 = float(input("Digite a 2 nota bimestral\n"))
num3 = float(input("Digite a 3 nota bimestral\n"))
num4 = float(input("Digite a 4 nota bimestral\n"))
med = (num1 + num2 + num3 + num4)/4
print(f'A média foi {med}.')

#5
def conv_m_cm(metros):
    return metros*100

num1 = float(input("Digite o valor em metros\n"))
print(f'O valor em centimetros é {conv_m_cm(num1)} ')
print(f'O valor em centimetros é {100*num1} ')
