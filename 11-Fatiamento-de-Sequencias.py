nome = 'Anne'
print(nome[0]) #'A'
len(nome) #4

#Acessar ultimo elemento
nome[len(nome)-1] #'e'
#OU
nome[-1]#'e'
nome[-2]#'n'

#Fatiamento é qd vc quer pegar só uma fatia da string

nome[0:3] # Pegar tres primeiros: 'Ann'
nome[-3:] #'nne'-- ja considera q termino no indice final da sequencia
nome[:2] #'An'  -- ja considera q comeca do indice 0

name = 'Kelly'
name[::-1] #'ylleK'

#tambem funciona com listas e tuplas

lista = list(range(10))
lista[-1] #9
lista[:3:2] #[0, 2] ate o 3 elemento e de dois em dois
lista[:8:2] #[0, 2, 4, 6] ate o oitavo elemento e de dois em dois

lista[::-1]
[9, 8, 7, 6, 5, 4, 3, 2, 1, 0]