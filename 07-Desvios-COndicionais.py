#Se liga no Escopo pela Identação

#IF
idade = 3
if idade < 18:
    print(17) 
    print(16)
#17
#16

#ELIF
if idade < 18:
    print(17) 
    print(16)
elif idade < 60:
    print(idade)
else:
    print("Idoso")