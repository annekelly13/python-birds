#Tupla se parece mt com a lista, mas usa () e ela é IMUTÁVEL, não se pode mudar ela.

tuple(range(6)) #(0, 1, 2, 3, 4, 5)

#Ha precedencia de operadores:
2+3*4 #14 

#para alterar isso, use parenteses
(2+3)*4 #20

(6) # essa operacao tem precedencia e Pyhton enxerga isso como inteiro
type(_) #<class 'int'>

(6,) #(6,) tupla de apenas 1 elemento
type(_) #<class 'tuple'>

#Atencao ao caso de Lista:
[6,] #[6]
type(_) #<class 'list'>


#Estruturas
registro = ('Anne', 22)
print(registro) #('Anne', 22)

#Desempacotamento
nome, idade = registro
print(nome) #'Anne'
print(idade) #22

#Ao tentar concatenar, ele cria uma nova tupla
registro_2 = ('kelly', 22)
print(registro + registro_2) #('Anne', 22, 'kelly', 22)

#ID unico dos objetos
id(registro_2)
id(registro)
id(registro + registro_2) 