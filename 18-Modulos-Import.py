#no matematica.py
def soma(parcela1, parcela2):
    return parcela1 + parcela2

#print(__name__) #dunder anne == __anne__ --- retorna o nome do modulo q esta sendo executado

if __name__ == '__main__': #para evitar q o codgio de teste seja executado qd importado dentro de outros modulos
    print(soma(1,2))

#no executar_matematica.py
import matematica #importa e executa o script matematica todo e n apenas a funcao soma desejada

print(matematica.soma(4,5))

