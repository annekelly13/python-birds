def soma(*parcelas):
     print(parcelas)
     print(type(parcelas))
 
soma()
# () 
#<class 'tuple'>


def soma(*parcelas):
    aux = 0
    for valor in parcelas:
        aux+= valor
    return aux

soma() #0
soma(2) #2
soma(2,4) #6
soma(2,4,10) #16


def f(**kwargs):
    print(kwargs)
    print(type(kwargs))

f()# {} <class 'dict'>

f(nome = 'Anne') 
#{'nome': 'Anne'} 
#<class 'dict'>

f(nome = 'Anne', sobrenome = 'Ribeiro') 
#{'nome': 'Anne', 'sobrenome': 'Ribeiro'} 
#<class 'dict'>


args = (2,4,10)
>>> kwargs = {'nome': 'Anne', 'sobrenome': 'Nuccitelli'}
>>> def f(*args, **kwargs):
    print(args)
    print(kwargs)

f()
#()
#{}

f(1,2,nome = 'Anne', sobrenome = 'Kelly')
#(1, 2)
#{'nome': 'Anne', 'sobrenome': 'Kelly'}


f(args, kwargs)
#((2, 4, 10), {'nome': 'Anne', 'sobrenome': 'Nuccitelli'})
#{}

f(*args) #um * para desempacotar uma tupla
#(2, 4, 10)
#{}

f(**kwargs) #dois * para desempacotar um dicionario
#()
#{'nome': 'Anne', 'sobrenome': 'Nuccitelli'}

f(*args, **kwargs)
#(2, 4, 10)
#{'nome': 'Anne', 'sobrenome': 'Nuccitelli'}


