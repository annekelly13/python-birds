1.35
type(1.35)
0.34

.1+.2 #0.30000000000000004   -- exatidao
0.1+0.2
0.2-0.3
-0.4

int(2.5) # 2 transforma float em int, trunca o mumero
float(2) # 2.0 transforma int em float

1j # numero complexo j
type(1j) #complex

1j**2 #-1+0j
(1+1j)**4 #(-4+0j)
