nome = "Anne"

for v in nome:
    print(v)
'''
A
n
n
e
'''


for i in range(len(nome)):
    print(i,nome[i])

'''
0 A
1 n
2 n
3 e
'''

#usando For com Desempacotamento
for i, v in enumerate(nome):
    print(i,nome[i])

'''
0 A
1 n
2 n
3 e
'''

#OU

for i,v in enumerate(nome):
    print(i,v)

'''
0 A
1 n
2 n
3 e
'''

