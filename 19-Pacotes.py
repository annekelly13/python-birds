#No mat.py (antigo matematica.py):

def soma(parcela1, parcela2):
    return parcela1 + parcela2

#print(__name__) #dunder anne == __anne__ --- retorna o nome do modulo q esta sendo executado

if __name__ == '__main__': #para evitar q o codgio de teste seja executado qd importado dentro de outros modulos
    print(soma(1,2))

#para executar na linha de comando, estando agora dentro de um pacote:
#python -m matematica.mat

#------------------------

#Em executar_matematica:

#import matematica.mat #importa e executa o script matematica todo e n apenas a funcao soma desejada

#from matematica import mat
#from matematica.mat import soma

from matematica.base.mat import soma as s #importa e chama a funcao soma por um apelido 's'
print(s(4,5))

#para executar na linha de comando, estando agora dentro de um pacote:
#python -m matematica.executar_matematica

