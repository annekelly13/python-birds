[1,2,3] #[1, 2, 3]
type([]) #<class 'list'>

#funcao range --> progressa aritmetica:
lista = list(range(10))
lista #[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

lista = list(range(1,10,2)) #lista [1, 3, 5, 7, 9]
lista = list(range(1,0,-2)) #lista [1]
lista = list(range(10,0,-2)) #lista [10, 8, 6, 4, 2]

lista.sort() #ordena os elementos de lista
lista.append(3) #adiciona 3 aos elementos da lista
lista.pop()#tira o ultimo elemento da lista e retorna ele
lista.extend([12,14]) #adiciona estes numeros no final da lista  [2, 3, 4, 6, 8, 12, 14]

lista+[16,18] #concatenar as listas == [2, 3, 4, 6, 8, 12, 14, 16, 18]
[1,3]*3 #[1,3,1,3,1,3]


#funcao split -- separa string numa lista separada por algo
'Python Pro'.split() #default é ser separado por espaco
'Python-Pro'.split('-')

lista=_ #lista =['Python', 'Pro']
'#'.join(lista) #'Python#Pro'

#Uma lista pode ser composta de qualquer objeto!
#Exemplo: [1, 1.0, 'Anne', []]