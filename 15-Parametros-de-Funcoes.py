#Se liga na fstring:

def ola(nome):
    return f'Ola {nome}'

def ola(nome, sobrenome):
    return f'Ola {nome} {sobrenome}'

ola('Anne', 'Ribeiro') #'Ola Anne Ribeiro

def ola(nome, sobrenome='Ribeiro'): #definindo um default p o parametro sobrenome
    return f'Ola {nome} {sobrenome}'

ola('Anne') #'Ola Anne Ribeiro'
ola('Anne', 'Kelly') #'Ola Anne Kelly'

def ola(nome, sobrenome='Ribeiro', idade='35'): #definindo um default p o parametro sobrenome
    return f'Ola {nome} {sobrenome} {idade}'


ola('Anne', 'Kelly', 30) #'Ola Anne Kelly 30'

ola('Anne', idade = 39) #'Ola Anne Kelly 39' --- a ordem dxa de ser relevante pq dxou explicito o nome do parametro